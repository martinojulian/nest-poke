import * as Joi from 'joi';

export const JoiValitationSchema = Joi.object({
  MONGODB: Joi.required(),
  PORT: Joi.number().default(6),
});
